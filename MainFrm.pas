unit MainFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms,
  StdCtrls, PsAPI, ComCtrls, XPMan, ExtCtrls;

type
  PUNICODE_STRING = ^UNICODE_STRING;
  UNICODE_STRING = record
    Length: Word;
    MaximumLength: Word;
    Buffer: PWideChar;
  end;

  POBJECT_ATTRIBUTES = ^OBJECT_ATTRIBUTES;
  OBJECT_ATTRIBUTES = record
    Length: DWORD;
    RootDirectory: Pointer;
    ObjectName: PUNICODE_STRING;
    Attributes: DWORD;
    SecurityDescriptor: Pointer;
    SecurityQualityOfService: Pointer;
  end;

  PCLIENT_ID = ^CLIENT_ID;
  CLIENT_ID = record
    UniqueProcess: DWORD;
    UniqueThread: DWORD;
  end;

  PPROCESS_BASIC_INFORMATION = ^PROCESS_BASIC_INFORMATION;
  PROCESS_BASIC_INFORMATION = record
    ExitStatus: Cardinal;
    PebBaseAddress: PBYTE;
    AffintyMask: DWORD;
    BasePriority: DWORD;
    UniqueProcessID: DWORD;
    InheritedFromUniqueProcessID: DWORD;
  end;

type
  TMainForm = class(TForm)
    Timer: TTimer;
    LV: TListView;
    XPManifest1: TXPManifest;
    Label1: TLabel;
    edCmdLine: TEdit;
    btCopy: TButton;
    Label2: TLabel;
    btKill: TButton;
    procedure FormCreate(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure LVSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure LVCompare(Sender: TObject; Item1, Item2: TListItem; Data: Integer;
      var Compare: Integer);
    procedure LVColumnClick(Sender: TObject; Column: TListColumn);
    procedure btCopyClick(Sender: TObject);
    procedure LVDblClick(Sender: TObject);
    procedure btKillClick(Sender: TObject);
  private
    { Private declarations }
    SortColumn: Integer;
    SortDirection: Integer;
    IsUpdating: Boolean;
    procedure ListTask;
  public
    { Public declarations }
  end;

function NtOpenProcess(Process: PHandle; dwAccessMask: DWORD; pObjectAttributes: POBJECT_ATTRIBUTES; pClientID: PCLIENT_ID): LongInt; stdcall; external 'ntdll.dll' name 'NtOpenProcess';
function NtQueryInformationProcess(Process: THandle; ProcessInformationClass: Integer; ProcessInformation: PPROCESS_BASIC_INFORMATION; ProcessInformationLength: Cardinal; var ReturnLength: Cardinal): LongInt; stdcall; external 'ntdll.dll' name 'NtQueryInformationProcess';

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

function GetProcCommandLine(const PID: Cardinal): String;
var
  R, RetLen, ObjLdr, ObjName: DWORD;
  hProcess: THandle;
  ObjAttr: OBJECT_ATTRIBUTES;
  ClientID: CLIENT_ID;
  ProcInfo: PROCESS_BASIC_INFORMATION;
  CmdLineW: array [0..MAX_PATH] of WideChar;
begin
  Result := '';

  ZeroMemory(@ObjAttr, sizeof(ObjAttr));
  ZeroMemory(@ClientID, sizeof(ClientID));

  ObjAttr.Length := sizeof(ObjAttr);
  ClientID.UniqueProcess := PID;

  R := NtOpenProcess(@hProcess, PROCESS_VM_READ or PROCESS_QUERY_INFORMATION, @ObjAttr, @ClientID);
  if FAILED(R) or (hProcess = 0) then exit;
  try
    R := NtQueryInformationProcess(hProcess, 0, @ProcInfo, sizeof(ProcInfo), RetLen);
    if (FAILED(R)) then exit;
    ReadProcessMemory(hProcess, Pointer(DWORD(ProcInfo.PebBaseAddress) + $10), @ObjLdr, 4, R);
    ReadProcessMemory(hProcess, Pointer(ObjLdr + $44), @ObjName, 4, R);
    ReadProcessMemory(hProcess, Pointer(ObjName), @CmdLineW, sizeof(CmdLineW), R);
    Result := CmdLineW;
  finally
    CloseHandle(hProcess);
  end;
end;

procedure SetPrivilege;
var
   OldTokenPrivileges, TokenPrivileges: TTokenPrivileges;
   ReturnLength: dword;
   hToken: THandle;
   Luid: Int64;
begin
   OpenProcessToken(GetCurrentProcess, TOKEN_ADJUST_PRIVILEGES OR TOKEN_QUERY, hToken);
   LookupPrivilegeValue(nil, 'SeDebugPrivilege', Luid);

   TokenPrivileges.PrivilegeCount := 1;
   TokenPrivileges.Privileges[0].Luid := Luid;
   TokenPrivileges.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;
   AdjustTokenPrivileges(hToken, False, TokenPrivileges, SizeOf(TTokenPrivileges), OldTokenPrivileges, ReturnLength);
   OldTokenPrivileges.Privileges[0].luid := Luid;
   OldTokenPrivileges.PrivilegeCount := 1;
   OldTokenPrivileges.Privileges[0].Attributes := TokenPrivileges.Privileges[0].Attributes or SE_PRIVILEGE_ENABLED;
   AdjustTokenPrivileges(hToken, False, OldTokenPrivileges, ReturnLength, PTokenPrivileges(nil)^, ReturnLength);
end;

procedure TMainForm.btCopyClick(Sender: TObject);
var
  H: Cardinal;
  P: PChar;
begin
  OpenClipboard(Handle);
  EmptyClipboard;
  H := GlobalAlloc(GMEM_MOVEABLE, Length(edCmdLine.Text) + 1);
  P := GlobalLock(H);
  lstrcpy(P, PChar(edCmdLine.Text));
  GlobalUnlock(H);
  SetClipboardData(CF_TEXT, H);
  CloseClipboard;
end;

procedure TMainForm.btKillClick(Sender: TObject);
var
  hProcess: THandle;
begin
  if (LV.Selected = NIL) then exit;
  if (MessageBox(Handle, 'Do you really want to terminate this process?', 'Confirm', MB_OKCANCEL OR MB_ICONQUESTION) <> IDOK) then exit;
  hProcess := OpenProcess(PROCESS_ALL_ACCESS, FALSE, Cardinal(LV.Selected.Data));
  TerminateProcess(hProcess, 3);
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  Application.Title := Caption;
  SetPrivilege;
  SortColumn := -1;
  SortDirection := 1;
  Scaled := FALSE;
  if (GetVersion AND $FF >= 6) then
  begin
    Font.Name := 'MS Shell Dlg';
    Font.Size := 8;
  end
  else
  begin
    Font.Name := 'MS Shell Dlg 2';
    Font.Size := 8;
  end;
//  TimerTimer(Sender);
  IsUpdating := FALSE;
  ListTask;
end;

procedure TMainForm.ListTask;
var
  I, NumOfProcs: Cardinal;
  PIDs: array [0..4095] of DWORD;
  BaseName: array [0..MAX_PATH] of char;
  hProcess: THandle;
  Item: TListItem;
begin
  EnumProcesses(@PIDs, sizeof(PIDs), NumOfProcs);
  NumOfProcs := NumOfProcs div sizeof(THandle);

  for I := 0 to NumOfProcs-1 do
  begin
    if I > LV.Items.Count-1 then
      Item := LV.Items.Add
    else
      Item := LV.Items[I];

    if (PIDs[I] = 0) then
      BaseName := 'Idle'
    else if (PIDs[I] = 4) then
      BaseName := 'System'
    else
    begin
      hProcess := OpenProcess(PROCESS_ALL_ACCESS, FALSE, PIDs[I]);
      ZeroMemory(@BaseName[0], MAX_PATH);
      GetModuleBaseName(hProcess, 0, BaseName, MAX_PATH);
      CloseHandle(hProcess);
    end;
    Item.Caption := BaseName;

    if Item.SubItems.Count > 0 then
    begin
      if (Item.Caption <> IntToStr(PIDs[I])) then
        Item.SubItems[0] := IntToStr(PIDs[I]);
    end
    else
      Item.SubItems.Add(IntToStr(PIDs[I]));

    if (Item.Data <> Pointer(PIDs[I])) then Item.Data := Pointer(PIDs[I]);
  end;
  for I := NumOfProcs to LV.Items.Count-1 do
  begin
    LV.Items.Delete(NumOfProcs);
  end;
end;

procedure TMainForm.LVColumnClick(Sender: TObject; Column: TListColumn);
begin
  if (Column <> NIL) then
  begin
    if (Column.Index = SortColumn) then SortDirection := -SortDirection;
    SortColumn := Column.Index;
  end;
  LV.AlphaSort;
end;

procedure TMainForm.LVCompare(Sender: TObject; Item1, Item2: TListItem; Data: Integer; var Compare: Integer);
begin
  case SortColumn of
  0: Compare := SortDirection * (CompareString(LOCALE_USER_DEFAULT, NORM_IGNORECASE, PChar(Item1.Caption), -1, PChar(Item2.Caption), -1) - 2);
  1: Compare := SortDirection * (Integer(Item1.Data) - Integer(Item2.Data));
  end;
end;

procedure TMainForm.LVDblClick(Sender: TObject);
begin
  btCopyClick(Sender);
end;

procedure TMainForm.LVSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
begin
  if (not Selected) or IsUpdating then exit;
  edCmdLine.Text := GetProcCommandLine(Cardinal(Item.Data));
end;

procedure TMainForm.TimerTimer(Sender: TObject);
var
  I: Integer;
  P: Pointer;
begin
  Timer.Enabled := FALSE;

  IsUpdating := TRUE;

  if (LV.Selected <> NIL) then
  begin
    P := LV.Selected.Data;
    LV.Selected.Focused := FALSE;
  end
  else
  begin
    P := Pointer(3); // process id cannot be 1
  end;

  LV.Items.BeginUpdate;
  ListTask;

  if (SortColumn <> -1) then
  begin
    LV.AlphaSort;
  end;

  for I := 0 to LV.Items.Count-1 do
  begin
    if (LV.Items[I].Data = P) then
    begin
      SendMessage(LV.Handle, $1019, I, 0);
      LV.Items[I].Selected := TRUE;
      LV.Items[I].Focused := TRUE;
      break;
    end;
  end;
  LV.Items.EndUpdate;

  IsUpdating := FALSE;

  Timer.Enabled := TRUE;
end;

end.
