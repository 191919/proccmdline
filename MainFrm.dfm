object MainForm: TMainForm
  Left = 262
  Top = 113
  Caption = 'Process Command Line Lister'
  ClientHeight = 590
  ClientWidth = 431
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    431
    590)
  PixelsPerInch = 120
  TextHeight = 16
  object Label1: TLabel
    Left = 8
    Top = 496
    Width = 90
    Height = 16
    Anchors = [akLeft, akBottom]
    Caption = 'Command &Line:'
    FocusControl = edCmdLine
    ExplicitTop = 295
  end
  object Label2: TLabel
    Left = 8
    Top = 560
    Width = 47
    Height = 16
    Caption = 'jh@6.cn'
    Enabled = False
  end
  object LV: TListView
    Left = 6
    Top = 8
    Width = 417
    Height = 481
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = 'Image Name'
        Width = 300
      end
      item
        Caption = 'PID'
      end>
    GridLines = True
    ReadOnly = True
    RowSelect = True
    ParentShowHint = False
    ShowHint = False
    TabOrder = 0
    ViewStyle = vsReport
    OnColumnClick = LVColumnClick
    OnCompare = LVCompare
    OnDblClick = LVDblClick
    OnSelectItem = LVSelectItem
  end
  object edCmdLine: TEdit
    Left = 8
    Top = 522
    Width = 415
    Height = 24
    Anchors = [akLeft, akRight, akBottom]
    ReadOnly = True
    TabOrder = 1
  end
  object btCopy: TButton
    Left = 348
    Top = 552
    Width = 75
    Height = 29
    Anchors = [akRight, akBottom]
    Caption = '&Copy'
    Default = True
    TabOrder = 2
    OnClick = btCopyClick
  end
  object btKill: TButton
    Left = 267
    Top = 552
    Width = 75
    Height = 29
    Anchors = [akRight, akBottom]
    Caption = '&Kill'
    TabOrder = 3
    OnClick = btKillClick
  end
  object Timer: TTimer
    Interval = 800
    OnTimer = TimerTimer
    Left = 576
    Top = 256
  end
  object XPManifest1: TXPManifest
    Left = 544
    Top = 256
  end
end
