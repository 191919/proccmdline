program ProcCmdLine;

uses
  Forms,
  Windows,
  SysUtils,
  MainFrm in 'MainFrm.pas' {MainForm};

{$R *.res}

var
  Sem: THandle;
  Buf: array [0..MAX_PATH] of char;
  AppName: String;
begin
  GetModuleFileName(0, Buf, MAX_PATH);
  AppName := StringReplace(LowerCase(Buf), '\', '_', [rfReplaceAll]);
  Sem := CreateSemaphore(NIL, 1, 1, PChar(AppName));
  if (GetLastError = ERROR_ALREADY_EXISTS) then exit;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;

  CloseHandle(Sem);
end.
